<?php

// src/AppBundle/Entity/Product.php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="product")
 */
class Product
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $name;

    /**
     * @ORM\Column(type="decimal", scale=2)
     */
    private $price;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\Column(type="text")
     */
    private $details;

    /**
     * @ORM\Column(type="string", length=2000)
     */
    private $imgurl1;


    /**
     * @ORM\Column(type="string", length=2000)
     */
    private $imgurl2;


    /**
     * @ORM\Column(type="string", length=2000)
     */
    private $imgurl3;

      /**
   * @ORM\ManyToOne(targetEntity="Category", inversedBy="products")
   * @ORM\JoinColumn(name="category_id", referencedColumnName="id")
   */
    private $category;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set name
     *
     * @param string $name
     *
     * @return Product
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set price
     *
     * @param string $price
     *
     * @return Product
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Product
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set category
     *
     * @param \AppBundle\Entity\Category $category
     *
     * @return Product
     */
    public function setCategory(\AppBundle\Entity\Category $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \AppBundle\Entity\Category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set imgurl1
     *
     * @param string $imgurl1
     *
     * @return Product
     */
    public function setImgurl1($imgurl1)
    {
        $this->imgurl1 = $imgurl1;

        return $this;
    }

    /**
     * Get imgurl1
     *
     * @return string
     */
    public function getImgurl1()
    {
        return $this->imgurl1;
    }

    /**
     * Set imgurl2
     *
     * @param string $imgurl2
     *
     * @return Product
     */
    public function setImgurl2($imgurl2)
    {
        $this->imgurl2 = $imgurl2;

        return $this;
    }

    /**
     * Get imgurl2
     *
     * @return string
     */
    public function getImgurl2()
    {
        return $this->imgurl2;
    }

    /**
     * Set imgurl3
     *
     * @param string $imgurl3
     *
     * @return Product
     */
    public function setImgurl3($imgurl3)
    {
        $this->imgurl3 = $imgurl3;

        return $this;
    }

    /**
     * Get imgurl3
     *
     * @return string
     */
    public function getImgurl3()
    {
        return $this->imgurl3;
    }

    /**
     * Set details
     *
     * @param string $details
     *
     * @return Product
     */
    public function setDetails($details)
    {
        $this->details = $details;

        return $this;
    }

    /**
     * Get details
     *
     * @return string
     */
    public function getDetails()
    {
        return $this->details;
    }

}
