<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Topic;
use AppBundle\Entity\Message;
use Doctrine\ORM\Query\ResultSetMapping;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use \Datetime;

class DefaultController extends Controller
{
    /**
    * @Route("/testpage/{id}/", name="testpage")
    */
    public function testPageAction($id)
    {

      $result = 'tested!';
      $html = file_get_contents('https://fineli.fi/fineli/api/v1/foods?q='.$id);
      $response = json_decode($html);

      $response = new JsonResponse($response);
      $response->headers->set("Access-Control-Allow-Origin", "*");

      return $response;
    }

    /**
     * @Route("/", name="homepage")
     */
    public function indexAction()
    {
        //return $this->render('default/forumindex.html.twig');
		$url = 'http://sym-h3677.rhcloud.com/forum/yleinen/page1';
		header('Location: ' . $url, true);
		die();
    }

    /**
     * @Route("/forum/{category}/{page}", name="homepageLonger")
     */
    public function indexLongerAction()
    {
        return $this->render('default/forumindex.html.twig');
    }

    /**
     * @Route("/forum/{category}/{page}/{subpage}", name="homepageLonger2")
     */
    public function indexLonger2Action()
    {
        return $this->render('default/forumindex.html.twig');
    }

    /**
     * @Route("/forum/{category}/{page}/{subpage}/{message}", name="homepageLonger3")
     */
    public function indexLonger3Action()
    {
        return $this->render('default/forumindex.html.twig');
    }

    /**
      * @Route("/restapi/{category}/", name="restIndexAction")
      */
      public function restIndexAction(Request $request, $category)
      {
        /*
       SELECT category_id FROM product LEFT JOIN category ON product.category_id=category.id WHERE product.name LIKE 'sam%'
                  */
     $resultPassword = $request->getContent();
     $rsm = new ResultSetMapping();
     $em = $this->getDoctrine()->getManager();
     $result = $em->getRepository('AppBundle:Topic')
       ->createQueryBuilder('u')
       ->where('u.category LIKE :category')
       ->andWhere('u.isDeleted = 0')
       ->setParameter('category', $category)
       ->orderBy('u.time', 'DESC')
       ->getQuery()
       ->getArrayResult();

       if(isset($_POST['password']) && !empty($_POST['password'])) {
         $password = $_POST['password'];
       }

       foreach ($result as $i => $product) {
         if ($password == $result[$i]['password']) {
            $result[$i]['enableDelete'] = 1;
         }
         $sql = "SELECT id, content, topic_id, password, time FROM message WHERE topic_id = '".$result[$i]['id']."' AND is_deleted = 0";

         $stmt = $em->getConnection()->prepare($sql);
         $stmt->execute();
         $categoryResults = $stmt->fetchAll();
         $result[$i]['replies'] = $categoryResults;
         foreach ($result[$i]['replies'] as $j => $product) {
           if ($password == $result[$i]['replies'][$j]['password']) {
             $result[$i]['replies'][$j]['enableDelete'] = 1;
           }
           unset($result[$i]['replies'][$j]['password']);
         }
         unset($result[$i]['password']);
       }

       return new JsonResponse(array('result' => $result));

      }

      /**
* @Route("/createtopic/", name="createproduct")
*/
public function createTopicAction(Request $request)
{

     $result = $request->getContent();
     $resultDecoded = json_decode($result);
     //$userId = $this->getUser();

     $date = new DateTime();

     $topic = new Topic();
     $topic->setName($resultDecoded->name);
     $topic->setContent($resultDecoded->content);
     $topic->setCategory($resultDecoded->category);
     $topic->setTime($date);
     $topic->setIsDeleted('0');
     $topic->setPassword($resultDecoded->password);

    $em = $this->getDoctrine()->getManager();
    $em->persist($topic);
    $em->flush();
    $topicId = $topic->getId();
    $topicTime = $topic->getTime();

    return new JsonResponse(array('id' => $topicId, 'time' => $topicTime));
}

/**
* @Route("/createreply/", name="createreply")
*/
public function createReplyAction(Request $request)
{

     $result = $request->getContent();
     $resultDecoded = json_decode($result);
     $userId = $this->getUser();
     $topicId = $resultDecoded->topic_id;

     $date = new DateTime();
     $em = $this->getDoctrine()->getManager();
     $topic = $em->getRepository('AppBundle:Topic')->find($topicId);

     $message = new Message();
     $message->setContent($resultDecoded->content);
     $message->setCategory($resultDecoded->category);
     $message->setTopic($topic);
     $message->setTime($date);
     $message->setIsDeleted('0');
     $message->setPassword($resultDecoded->password);

    $em = $this->getDoctrine()->getManager();
    $em->persist($message);
    $em->flush();
    $messageId = $message->getId();
    $messageTime = $message->getTime();

    return new JsonResponse(array('id' => $messageId, 'time' => $messageTime));

}

  /**
  * @Route("/deletetopic/{id}", name="deleteTopicAction")
  */
  public function deleteTopicAction(Request $request, $id)
  {
    /*
   SELECT category_id FROM product LEFT JOIN category ON product.category_id=category.id WHERE product.name LIKE 'sam%'
              */

    $result = $request->getContent();
    $resultDecoded = json_decode($result);
    $password = $resultDecoded->password;

    $em = $this->getDoctrine()->getManager();
    $topic = $em->getRepository('AppBundle:Topic')->find($id);

    $topicPassword = $topic->getPassword();
    if ($topicPassword == $password) {
      $topic->setIsDeleted(1);
      $em->flush();
      $json = '{"status": 1, "result": "Delete done"}';
    } else {
      $json = '{"status": 0, "result": "Password incorrect!"}';
    }

    $result = json_decode($json);
    return new JsonResponse(array('result' => $result));

  }

  /**
  * @Route("/deletemessage/{id}", name="deleteMessageAction")
  */
  public function deleteMessageAction(Request $request, $id)
  {
    /*
   SELECT category_id FROM product LEFT JOIN category ON product.category_id=category.id WHERE product.name LIKE 'sam%'
              */

    $result = $request->getContent();
    $resultDecoded = json_decode($result);
    $password = $resultDecoded->password;

    $em = $this->getDoctrine()->getManager();
    $message = $em->getRepository('AppBundle:Message')->find($id);

    $messagePassword = $message->getPassword();

    if ($password == $messagePassword) {
      $message->setIsDeleted(1);
      $em->flush();

      $json = '{"status": 1, "result": "Delete done"}';

    } else {
      $json = '{"status": 0, "result": "Password incorrect!"}';
    }

    $result = json_decode($json);
    return new JsonResponse(array('result' => $result));

  }

  /**
  * @Route("/editTopic/{id}", name="editTopicAction")
  */
  public function editTopicAction(Request $request, $id)
  {
    /*
   SELECT category_id FROM product LEFT JOIN category ON product.category_id=category.id WHERE product.name LIKE 'sam%'
              */

    $result = $request->getContent();
    $resultDecoded = json_decode($result);
    $password = $resultDecoded->password;

    $em = $this->getDoctrine()->getManager();
    $message = $em->getRepository('AppBundle:Topic')->find($id);

    $messagePassword = $message->getPassword();

    if ($password == $messagePassword) {
      $message->setContent($resultDecoded->content);
      $em->flush();

      $json = '{"status": 1, "result": "Edit done"}';

    } else {
      $json = '{"status": 0, "result": "Password incorrect!"}';
    }

    $result = json_decode($json);
    return new JsonResponse(array('result' => $result));

  }

     /**
      * @Route("/restapi1/{category}/", name="showproduct")
      */
      public function showAction($category)
      {

      $em = $this->getDoctrine()->getManager();
      $sql = "SELECT * FROM `topic`";

      try {
          $stmt = $em->getConnection()->prepare($sql);
          $stmt->execute();
          $results = $stmt->fetchAll();

          foreach ($results as $i => $product) {
               $sql = "SELECT id, content, topic_id, time FROM message WHERE topic_id = '".$results[$i]['id']."' AND is_deleted = 0";

               $stmt = $em->getConnection()->prepare($sql);
               $stmt->execute();
               $categoryResults = $stmt->fetchAll();
               $results[$i]['replies'] = $categoryResults;
          }

          return new JsonResponse(array('result' => $results));
      } catch (\Exception $e) {
          // failed to connect
          return new Response('Exception is: '.$e);
      }

        return new Response('Name is: ');
      }


      /**
       * @Route("/topic/{id}/", name="viewTopic")
       */
       public function viewAction($id)
       {
         $em = $this->getDoctrine()->getManager();
         $topic = $em->getRepository('AppBundle:Topic')->find($id);

         return new Response('Password is: '.$topic->getPassword());
       }
}
