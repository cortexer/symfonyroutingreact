var _u = _.noConflict();

var NoteSection = React.createClass({
    getInitialState: function() {
        return {
            notes: [],
            allNotes: [],
            buttonPages: [],
            filter: false,
            editableTopic: null,
            currentContent: null,
            password: localStorage.getItem('password'),
            subPageIndex: 0
        }
    },

    onBackButtonEvent: function(event) {
      if(event){
        event.preventDefault();
      } if (this.state.filter) {
        this.toggleTopic(event);
        return
      } if (!this.state.filter) {
        var url = window.location.href.toString();
        var res = url.split("/");
        if (res[6]) {
          history.pushState('data to be passed', 'Title of the page', '/forum/yleinen/' + this.state.prevPage);
          return
        }
        if (url.includes('page')){
          var pageParam = url.substring(url.indexOf("page") + 4);
          pageParam = pageParam - 1;
          pageParam = pageParam * 5;
          var pageParam2 = pageParam + 5;
          var allNotes = this.state.allNotes;
          var newNotes = allNotes.slice(pageParam, pageParam2);
          this.setState({notes: newNotes});
        }
      }
    },

    componentDidMount: function() {
        //var password = localStorage.getItem('password');
        //if (password) { this.setState({password: JSON.parse(password)}); }
        this.loadNotesFromServer();
        window.onpopstate = this.onBackButtonEvent;
        //setInterval(this.loadNotesFromServer, 2000);
        var password = this.state.password;
        if (password) {
            password = password.replace(/['"]+/g, '');
            this.setState({password: password});
        }
    },

    changePasswordAndGetPosts: function() {
      localStorage.setItem('password', JSON.stringify(this.state.password));
      this.loadNotesFromServer(0, 5);
    },

    loadNotesFromServer: function(first, last, url) {
      var self = this;
      var password = this.state.password;
      if (password) {
          password = password.replace(/['"]+/g, '');
      }
      if (!url) {
        var url = this.props.url;
        var res = url.split("/");
        if (res[5] && !res[5].includes('page')) {
          var topicId = res[5];
          url = url.replace(topicId, "");
        }
      }
        $.ajax({
            url: url,
            type: 'POST',
            data: {password: password},
            success: function (result) {
              this.setState({allNotes: result.result});
              self.setState({allNotes: result.result});
              var emptyArray = [];
              this.setState({buttonPages: emptyArray});
              var buttonPagesAmount = result.result.length / 5;
              var buttonPages = this.state.buttonPages;
              buttonPagesAmount = Math.ceil(buttonPagesAmount);
              var firstPage = -5;
              var lastPage = 0;
              for (var i = 0; i < buttonPagesAmount; i++) {
                firstPage = firstPage + 5;
                lastPage = lastPage + 5;
                var buttonPage = {
                  id: i,
                  firstPage: firstPage,
                  lastPage: lastPage
                };
              buttonPages.push(buttonPage);
         }
            result = result.result.slice(first, last);
            this.setState({buttonPages: buttonPages});
            this.setState({notes: result});
            this.forceUpdate();
            }.bind(this)
        }).then(function(result) {
         var url = window.location.href.toString();
         var res = url.split("/");
         if (url.includes('page')){
           var pageParam = url.substring(url.indexOf("page") + 4);
           var pageParam = pageParam - 0;
           pageParam = pageParam * 5;
           pageParam = pageParam - 5;
           self.changePages(pageParam);
         } if(!res[5]){
           history.pushState('data to be passed', 'Title of the page', '/forum/yleinen/' + 'page' + 1);
         } if(res[5] && !res[5].includes('page')){
           var notes = self.state.allNotes;
           var currentNote = _u.find(notes, {id:Number(res[5])});
           self.setState({filter: true, notes: [currentNote]});
         }
         if (res[6]) {
           history.pushState('data to be passed', 'Title of the page', url);
           var numb = res[6].match(/\d/g).toString();
           self.setState({subPageIndex: numb});
         }
     });
    },


    createTopic: function(event) {
    event.preventDefault();


  	var object = {
  	 _id: '',
     name: this.state.topicName,
  	 content: this.state.topicContent,
     category: 'yleinen',
     password: this.state.password
  	};
    var self = this;
    fetch('/createtopic/', {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Cache': 'no-cache'
        },
        body: JSON.stringify({
          name: object.name,
          content: object.content,
          category: object.category,
          password: object.password
        }),
        credentials: 'include'
        }).then((response) => response.json())
        .then((responseJson) => {
        object.id = responseJson.id;
        object.user = 'user';
        object.time = responseJson.time;
        object.replies = [];
        object.enableDelete = 1;
        object.isDeleted = 0;
        self.state.allNotes.unshift(object);
        var notes = self.state.allNotes.slice(0, 5);
        self.setState({ allNotes: self.state.allNotes, notes: notes});

        var emptyArray = [];
        self.setState({buttonPages: emptyArray});
        var buttonPagesAmount = self.state.allNotes.length / 5;
        buttonPagesAmount = Math.ceil(buttonPagesAmount);
        var buttonPages = [];
        var firstPage = -5;
        var lastPage = 0;
        for (var i = 0; i < buttonPagesAmount; i++) {
          firstPage = firstPage + 5;
          lastPage = lastPage + 5;
          var buttonPage = {
            id: i,
            firstPage: firstPage,
            lastPage: lastPage
          };
        buttonPages.push(buttonPage);
      }
      self.setState({buttonPages: buttonPages});
        var url = window.location.href.toString();
        var res = url.split("/"); //last url parameter (page/topic) is res[5]
        url = res[3]+"/"+res[4]+"/page"+ 1;
        window.history.pushState("object or string", "Title", "/" + url);
        self.forceUpdate();
      });
  },

  replyToTopic: function(event) {
  event.preventDefault();
  var noteId = this.state.notes[0].id;
  var d = new Date();
  var n = d.getTime();

  var note = _u.find(this.state.allNotes, {id:noteId});

  var object = {
   _id: '',
   content: this.state.topicName,
   category: 'yleinen',
   topic_id: noteId,
   password: this.state.password
  };
  var self = this;
  fetch('/createreply/', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Cache': 'no-cache'
      },
      body: JSON.stringify({
        name: object.name,
        content: object.content,
        category: 'yleinen',
        topic_id: noteId,
        password: this.state.password
      }),
      credentials: 'include'
      }).then((response) => response.json())
      .then((responseJson) => {
      object.id = responseJson.id;
      object.user = responseJson.user;
      object.time = responseJson.time;
      object.enableDelete = 1;
      note.replies.push(object);
      var match = _u.merge(self.state.allNotes, note);
      self.setState({ allNotes: self.state.allNotes, editableTopic: null, editedContent: null });
      var pagesNumber = self.state.notes[0].replies.length;
      pagesNumber = Math.ceil(pagesNumber/5);
      var url = window.location.href.toString();
      var res = url.split("/"); //last url parameter (page/topic) is res[5]
      url = res[3]+"/"+res[4]+"/"+res[5]+"/page"+ pagesNumber;
      window.history.pushState("object or string", "Title", "/" + url);
      self.forceUpdate();
    });
},

  openEditForm: function(topicId, topicContent) {
  this.setState({ editableTopic: topicId, currentContent: topicContent});
},

  editTopic: function(e) {
    e.preventDefault();
    var editableTopic = this.state.editableTopic;
    var editedContent = this.state.currentContent;

  var object = {
   content: editedContent,
   password: this.state.password
  };
  var self = this;
    fetch('/editTopic/' + editableTopic, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Cache': 'no-cache'
      },
      body: JSON.stringify({
        content: object.content,
        password: object.password
      }),
      credentials: 'include'
      }).then((response) => response.json())
      .then((responseJson) => {
      if (responseJson.result.status == 1) {
      var note = _u.find(self.state.allNotes, {id:editableTopic});
      note.content = object.content;
      var match = _u.merge(self.state.allNotes, note);
      self.setState({ allNotes: self.state.allNotes, editableTopic: null, editedContent:null });
    } else {
      alert(JSON.stringify(responseJson.result.result));
    }
    });

},

  deleteTopic: function(id) {
    var self = this;
    fetch('/deletetopic/' + id, {
          method: 'DELETE',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            password: self.state.password
          }),
          credentials: 'include'
        }).then((response) => response.json())
        .then((responseJson) => {
        if (responseJson.result.status == 1) {
          _u.remove(this.state.allNotes, reply => reply.id === id);
          var notes = this.state.allNotes;
          notes = notes.slice(0, 5);
          self.setState({filter: false, notes: notes, pageNumber: 0});
          var url = window.location.href.toString();
          var res = url.split("/"); //last url parameter (page/topic) is res[5]
          url = res[3]+"/"+res[4]+"/page"+ 1;
          window.history.pushState("object or string", "Title", "/" + url);
          self.forceUpdate();
        } else {
          alert(JSON.stringify(responseJson.result.result));
        }
      });
  },

  deleteMessage: function(id, noteId) {
    var self = this;
    fetch('/deletemessage/' + id, {
          method: 'DELETE',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            password: self.state.password
          }),
          credentials: 'include'
        }).then((response) => response.json())
        .then((responseJson) => {
        if (responseJson.result.status == 1) {
          noteId = Number(noteId);
          var notes = _u.find(this.state.allNotes, {id:noteId});
          _u.remove(notes.replies, reply => reply.id === id);
          var match = _u.merge(this.state.allNotes, notes);
          if(match){
            this.setState({ allNotes: this.state.allNotes });
          }
          var url = window.location.href.toString();
          var res = url.split("/"); //last url parameter (page/topic) is res[5]
          if (self.state.filter) {
            url = res[3]+"/"+res[4]+"/"+res[5]+"/page"+ 1;
          } else {
            url = res[3]+"/"+res[4]+"/"+res[5];
          }
          window.history.pushState("object or string", "Title", "/" + url);
          self.forceUpdate();
        } else {
          alert(JSON.stringify(responseJson.result.result));
        }
      });
  },

    toggle: function() {
        {this.state.isModalOpen ? this.setState({isModalOpen: false}) : this.setState({isModalOpen: true})}
    },

    updatePassword: function(e) {
        this.setState({password: e.target.value});
    },

    updateTopicName: function(e) {
        this.setState({topicName: e.target.value});
    },

    updateTopicContent: function(e) {
        this.setState({topicContent: e.target.value});
    },

    updateContent: function(e) {
        this.setState({currentContent: e.target.value});
    },

    toggleTopic: function(event) {
      if(!this.state.filter){
        var url = window.location.href.toString();
        var res = url.split("/"); //last url parameter (page/topic) is res[5]
        var topicId = Number(event.target.value);
        var currentTodo = _u.find(this.state.notes, {id:topicId});
        this.setState({prevNotes: this.state.notes});
        this.setState({prevPage: res[5]});
        this.setState({filter: true});
        this.setState({notes: [currentTodo]});
        history.pushState('data to be passed', 'Title of the page', '/forum/yleinen/' + topicId + '/page1');
      } if(this.state.filter){
        var url = window.location.href.toString();
        var res = url.split("/");
        if (res[6] && res[6].includes('page')) {
          this.forceUpdate();
          return
        } else {
          this.setState({filter: false});
          if (!this.state.prevNotes) {
            var firstNotes = this.state.allNotes;
            firstNotes = firstNotes.slice(0, 5);
            this.setState({notes: firstNotes});
            history.pushState('data to be passed', 'Title of the page', '/forum/yleinen/' + 'page1');
            return
          }
        }
        this.setState({notes: this.state.prevNotes});
        history.pushState('data to be passed', 'Title of the page', '/forum/yleinen/' + this.state.prevPage);
      }
    },

    toggleTopicFromButton: function(event) {
      if(!this.state.filter){
        var url = window.location.href.toString();
        var res = url.split("/"); //last url parameter (page/topic) is res[5]
        var topicId = Number(event.target.value);
        // var currentTodo = _u.find(this.state.notes, {id:topicId}); Lodash way, but stopped working for some reason
        var currentTodo = null;
        for (var i = 0; i < this.state.notes.length; i++) {
            if (this.state.notes[i].id == topicId) {
              var currentTodo = this.state.notes[i];
            }
        }
        this.setState({prevNotes: this.state.notes});
        this.setState({prevPage: res[5]});
        this.setState({filter: true});
        this.setState({notes: [currentTodo]});
        history.pushState('data to be passed', 'Title of the page', '/forum/yleinen/' + topicId + '/page1');
      } if(this.state.filter){
          this.setState({filter: false});
          if (!this.state.prevNotes) {
            var firstNotes = this.state.allNotes;
            firstNotes = firstNotes.slice(0, 5);
            this.setState({notes: firstNotes});
            history.pushState('data to be passed', 'Title of the page', '/forum/yleinen/' + 'page1');
            return
        }
        this.setState({notes: this.state.prevNotes});
        history.pushState('data to be passed', 'Title of the page', '/forum/yleinen/' + this.state.prevPage);
        this.loadNotesFromServer(0, 5); // check for bugs
      }
    },

    changePages: function(pageNumberFirst) {
      var newNotes = this.state.allNotes;
      newNotes = newNotes.slice(pageNumberFirst, pageNumberFirst + 5);
      this.setState({notes: newNotes, pageNumber: pageNumberFirst});
      var pageNumber = parseInt(pageNumberFirst/5)+1;
      history.pushState('data to be passed', 'Title of the page', '/forum/yleinen/' + 'page' + pageNumber);
  },
    render: function() {
        return (
            <div>
                <div className="notes-container">
                  <p>Password (for deleting messages):</p>
                  <input className="form-control" value = {this.state.password} onChange = {this.updatePassword} type="name" ref = "passwordInput" placeholder="name" maxLength="50"></input>
                  <button onClick={this.changePasswordAndGetPosts} className="btn btn-default">Change Password</button>
                  <h2 className="notes-header">Forum Topics</h2>
                    <div><i className="fa fa-plus plus-btn"></i></div>
                </div>
                <NoteTest filter={this.state.filter} updateContent={this.updateContent} updateTopicContent={this.updateTopicContent} updateTopicName={this.updateTopicName} isModalOpen={this.state.isModalOpen} toggle={this.toggle} createTopic={this.createTopic} replyToTopic={this.replyToTopic} editTopic={this.editTopic} editableTopic={this.state.editableTopic} currentContent={this.state.currentContent}/>
                <NoteList subPageIndex={this.state.subPageIndex} pageNumber={this.state.pageNumber} filter={this.state.filter} editTopic={this.editTopic} openEditForm={this.openEditForm} deleteTopic={this.deleteTopic} deleteMessage={this.deleteMessage} toggleTopic={this.toggleTopic} toggleTopicFromButton={this.toggleTopicFromButton} changePages={this.changePages} notes={this.state.notes} buttonPages={this.state.buttonPages} />
            </div>
        );
    }
});

var NoteTest = React.createClass({
  componentWillMount: function() {
    this.state = {
        isModalOpen: this.props.isModalOpen,
        editableTopic: null,
        currentContent: null,
        filter: false
    };
},

  componentWillReceiveProps: function(nextProps: Props) {
      if (nextProps) {
          this.setState({
              isModalOpen: nextProps.isModalOpen,
              filter: nextProps.filter,
              editableTopic: nextProps.editableTopic,
              currentContent: nextProps.currentContent
          });
      }
  },

    render: function() {
        return (
          <div style={{border: '1px solid black', marginBottom: '20px'}}>
            <button onClick = {this.props.toggle} className="btn btn-default">{!this.state.filter ? 'Add a new topic' : 'Write a reply'}</button>
              {this.state.isModalOpen && !this.state.filter ?
                <div><form>
                <table>
                  <tbody>
                <tr>
                  <td style={{paddingRight: '10px'}}>Topic name:</td>
                  <td><input className="form-control" value = {this.state.name} onChange = {this.props.updateTopicName} type="name" ref = "nameInput" placeholder="name" maxLength="5000"></input></td>
                </tr>
              </tbody>
              <tbody>
                <tr>
                  <td style={{paddingRight: '10px'}}>Topic content:</td>
                  <td><textarea className="form-control" value = {this.state.description} onChange = {this.props.updateTopicContent} type="name" ref = "descriptionInput" placeholder="description" maxLength="140"></textarea></td>
                </tr>
                </tbody>
                </table>
                <button onClick={this.props.createTopic} className="btn btn-default">Create</button>
                </form></div> : ''}

                {this.state.editableTopic ?
                  <div><form>
                  <table>
                    <tbody>
                  <tr>
                    <td>Item name</td>
                    <td><textarea className="form-control" value = {this.state.currentContent} onChange = {this.props.updateContent} type="name" ref = "nameInput" placeholder="name" maxLength="150"></textarea></td>
                  </tr>
                </tbody>
                  </table>
                  <button onClick={this.props.editTopic} className="btn btn-default">Edit</button>
                  </form></div>
                 : ''}

                 {this.state.isModalOpen && this.state.filter ?
                   <div><form>
                   <table>
                     <tbody>
                   <tr>
                     <td style={{paddingRight: '10px'}}>Post content:</td>
                     <td><input className="form-control" value = {this.state.name} onChange = {this.props.updateTopicName} type="name" ref = "nameInput" placeholder="name" maxLength="5000"></input></td>
                   </tr>
                 </tbody>
                   </table>
                   <button onClick={this.props.replyToTopic} value={this.props.editableTopic} className="btn btn-default">Reply</button>
                   </form></div> : ''}

            </div>
        );
    }
});

var NoteList = React.createClass({
  componentWillReceiveProps: function(nextProps: Props) {
      if (nextProps) {
          this.setState({
              filter: nextProps.filter,
              notes: nextProps.notes,
              subPageIndex: nextProps.subPageIndex
          });
      }
  },

    renderReplies: function() {
      return (
          <section id="cd-timeline">

          </section>
      );

    },
    render: function() {
      var self = this;

      for (var i = 0; i < this.props.notes.length; i++) {
          this.props.notes[i].time.date = this.props.notes[i].time.date.replace(".000000", "");
      }

        var noteNodes = this.props.notes.map(function(note) {
            return (
                <div key={note.id} style={{padding: '15px', border: '1px solid #02B097', borderRadius: '5px'}}>
                <NoteBox deleteTopic={self.props.deleteTopic.bind(null, note.id, self.props.noteId)} openEditForm={self.props.openEditForm.bind(null, note.id, note.content)} enableDelete={note.enableDelete} noteId={note.id} noteName={note.name} noteContent={note.content} subPageIndex={self.state.subPageIndex} filter={self.state.filter} deleteMessage={self.props.deleteMessage} toggleTopic={self.props.toggleTopic} toggleTopicFromButton={self.props.toggleTopicFromButton} id={note.id} date={note.time.date} key={note.id+1} replies={note.replies}>
                /*  {self.state.filter ?
                    <div>
                      {note.enableDelete ? <div><button key={note.id+1} onClick = {self.props.deleteTopic.bind(null, note.id, self.props.noteId)} className="btn btn-default">Delete</button>
                      <button key={note.id+1.5} onClick = {self.props.openEditForm.bind(null, note.id, note.content)} className="btn btn-default">Edit</button></div> : ''}
                    </div>
                   : ''} */
                {note.user}<br/><div style={{padding: '5px', border: '1px solid black', borderRadius: '5px'}}></div><br/>{note.content}</NoteBox></div>
            );
        });
        var url = window.location.href.toString();
        var res = url.split("/");
        if (res[5]) {
          var number = res[5].replace( /^\D+/g, '');
          number = number * 5;
        }

        var buttonPages = this.props.buttonPages.map(function(button) {
          button.active = 0;
          if (number == button.lastPage) {
              button.active = 1;
          }

          if (!self.state.filter) {
            return (
                <button key={button.id+1} onClick={self.props.changePages.bind(null, button.firstPage, button.lastPage)} className="btn btn-default" style={(button.active) ? {border: '1px solid black'} : {display: 'true'}}>{button.firstPage} - {button.lastPage}</button>
            );
          }
        });

        return (
            <section id="cd-timeline">
                {noteNodes}
                {buttonPages}
            </section>
        );
    }
});

var NoteBox = React.createClass({
  componentWillMount: function() {
    this.state = {
        allNoteReplies: this.props.replies,
        noteReplies: this.props.replies,
        subPageIndex: this.props.subPageIndex
    };
    //  var url = window.location.href.toString();
    //  var res = url.split("/");
    //  history.pushState('data to be passed', 'Title of the page', '/forum/yleinen/52' + 'page' + 1);
    var noteReplies = this.state.allNoteReplies;
    //noteReplies = noteReplies.slice(0, noteReplies.length);
    //this.setState({noteReplies: noteReplies});
    //var firstSubPage = Number(this.props.subPageIndex);

  },
  componentWillReceiveProps: function(nextProps: Props) {
      if (nextProps) {
          this.setState({
              filter: nextProps.filter,
              notes: nextProps.notes,
              subPageIndex: nextProps.subPageIndex,
              noteReplies: nextProps.replies
          });
      }
  },
  changeSubPages: function(first, last) {
      var url = window.location.href.toString();
      var res = url.split("/"); //last url parameter (page/topic) is res[5]
      var urlInt = (first / 5) + 1;
      if (urlInt == 0) {
        urlInt = 1;
      }
      url = res[3]+"/"+res[4]+"/"+res[5]+"/page"+ urlInt;
      window.history.pushState("object or string", "Title", "/" + url);
      this.forceUpdate();
  },
  highlightPost: function(first, last) {
    var url = window.location.href.toString();
    var res = url.split("/"); //last url parameter (page/topic) is res[5]
    url = res[3]+"/"+res[4]+"/"+res[5]+"/"+res[6]+"/"+first;
    window.history.pushState("object or string", "Title", "/" + url);
    this.forceUpdate();
  },
    render: function() {
      var url = window.location.href.toString();
      var res = url.split("/");
      if (res[6]) {
        var number = res[6].replace( /^\D+/g, '');
        number = number * 5;
      }
      if (!res[6] && !number) {
        var number = 5;
      }
      if (res[5] && !res[6]) {
        var number = res[5].replace( /^\D+/g, '');
        number = number * 5;
      }
      if (res[7]) {
        var highlightedPost = res[7];
      }
      //var allNoteReplies = this.props.replies;
      var noteReplies = this.state.noteReplies;
      if(!this.props.filter && noteReplies.length >= 3){
        //noteReplies = _u.takeRight(noteReplies, 3); //lodash way, makes "&& noteReplies.length > 2" not needed
        if (noteReplies.length > 3) {
          noteReplies = noteReplies.slice(Math.max(noteReplies.length - 3, 1));
        }
        if (noteReplies.length <= 3) {
          noteReplies = noteReplies;
        }
      } else if (this.props.filter) {
        if (number) {
          noteReplies = noteReplies.slice(number-5, number);
        } else {
          noteReplies = noteReplies.slice(0, 5);
        }
      }

      var self = this;
      noteReplies = noteReplies.map(function(note) {
          return (
            <div style={{padding: '10px', minHeight: '100px', width: '550px', border: '1px dotted lightBlue', borderRadius: '5px', wordWrap: 'break-word'}} key={note.id}>
              <div style={(note.id == highlightedPost) ? {padding: '10px', minHeight: '100px', width: '530px', border: '3px dotted black', borderRadius: '5px', wordWrap: 'break-word'} : {border: ''}}>
              {note.enableDelete ? <button key={note.id} onClick = {self.props.deleteMessage.bind(null, note.id, note.topic_id)} className="btn btn-default">Delete</button> : ''}
              <p style={{padding: '2px', paddingLeft: '-5px', marginRight: '15px', border: '1px solid black', float: 'left'}}>{self.props.filter ? <button key={note.id} onClick={self.highlightPost.bind(null, note.id, note.topic_id)} className="btn btn-default">{note.id}</button> : ''}Reply</p>
              <p style={{paddingTop: '-150px'}}>{note.content}</p>
            </div></div>
          );
      });

      var replyButtons = [];
      var buttonPagesAmount = this.state.allNoteReplies.length / 5;
      buttonPagesAmount = Math.ceil(buttonPagesAmount);
      var firstPage = -5;
      var lastPage = 0;
      for (var i = 0; i < buttonPagesAmount; i++) {
        firstPage = firstPage + 5;
        lastPage = lastPage + 5;
        var buttonPage = {
          id: i,
          firstPage: firstPage,
          lastPage: lastPage
        };
      replyButtons.push(buttonPage);
      }
      var self = this;
      var replyButtons = replyButtons.map(function(button) {
        button.active = 0;
        if (number == button.lastPage) {
          button.active = 1;
        }
        if (self.props.filter) {
          return (
              <button key={button.id} onClick={self.changeSubPages.bind(null, button.firstPage, button.lastPage)} className="btn btn-default" style={(button.active) ? {border: '1px solid black'} : {display: 'true'}}>{button.firstPage} - {button.lastPage}</button>
          );
        }
      });
        return (
            <div className="cd-timeline-block">
                <div className="cd-timeline-img">
                    <img src={this.props.avatarUri} className="img-circle" alt="" />
                </div>
                <div className="cd-timeline-content">
                    <button style={{float: 'right', border: "1px solid black"}} onClick = {this.props.toggleTopicFromButton} value={this.props.id} className="btn btn-default">{self.props.filter ? 'Return (and get new posts)' : 'Open topic' } {this.props.id}</button>
                    {/* {self.props.filter ? Uncomment...
                        <div> */}
                          {this.props.enableDelete ? <div><button onClick={self.props.deleteTopic} className="btn btn-default">Delete</button>
                          <button key={this.props.id} onClick={self.props.openEditForm} className="btn btn-default">Edit</button></div> : ''}
                    {/*    </div> ...to show only when the topic is open
                       : ''} */}
                    <h3>{this.props.noteName}</h3>
                    <p>{this.props.noteContent}</p>
                    <span className="cd-date">{this.props.date}</span>
                      {self.props.filter ?
                      <div style={{padding: '2px', maxWidth: '200px', border: "1px solid black", borderRadius: '5px'}}><p>Click the reply id to highlight it!</p></div>
                       : ''}
                </div>
                {noteReplies}
                {replyButtons}
            </div>
        );
    }
});

window.NoteSection = NoteSection;